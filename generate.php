<?php
/**
 *	This file creates the single use download link
 *	The query string should be the password (which is set in variables.php)
 *	If the password fails, then 404 is rendered
 *
 *	Expects: generate.php?1234
 */

date_default_timezone_set('UTC');
// The actual file (path starts from this directory forward)
define('PROTECTED_DOWNLOAD','/fiel/location/filename.zip');

// The path to the download.php file (probably same dir as this file)
define('DOWNLOAD_PATH','download.php');

// What the file will be displayed to users as
define('SUGGESTED_FILENAME','download.zip');

// The admin password to generate a new download link
define('ADMIN_PASSWORD','1234');

// The expiration date of the link (examples: +1 year, +5 days, +13 hours)
define('EXPIRATION_DATE', '+48 hours');

// Don't worry about this
define('CONTENT_TYPE','application/zip');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: ".date('U', strtotime(EXPIRATION_DATE)));

// Grab the query string as a password
#$password = trim($_SERVER['QUERY_STRING']);
// Use static password (One or the other option)
$password = "1234";
/*
 *	Verify the admin password (in variables.php)
 */
if($password == ADMIN_PASSWORD) {
    // Create a new key
    $new = uniqid('key',TRUE);

    /*
     *	Create a protected directory to store keys in
     */
    if(!is_dir('keys')) {
        mkdir('keys');
        $file = fopen('/keys/.htaccess','w');
        fwrite($file,"Order allow,deny\nDeny from all");
        fclose($file);
    }

    /*
     *	Write the key key to the keys list
     */
    $file = fopen('/keys/keys','a');
    fwrite($file,"{$new}\n");
    fclose($file);

    echo "http://" . $_SERVER['HTTP_HOST'] . DOWNLOAD_PATH . "?" . $new;


} else {
    /*
     *	Someone stumbled upon this link with the wrong password
     *	Fake a 404 so it does not look like this is a correct path
     */
    header("HTTP/1.0 404 Not Found");
}
?>