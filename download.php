<?php
/**
 *	This file does the actual downloading
 *	It will take in a query string and return either the file,
 *	or failure
 *
 *	Expects: download.php?key1234567890
 */
date_default_timezone_set('UTC');
// The actual file (path starts from this directory forward)
define('PROTECTED_DOWNLOAD','/fiel/location/filename.zip');

// The path to the download.php file (probably same dir as this file)
define('DOWNLOAD_PATH','download.php');

// What the file will be displayed to users as
define('SUGGESTED_FILENAME','download.zip');

// The admin password to generate a new download link
define('ADMIN_PASSWORD','1234');

// The expiration date of the link (examples: +1 year, +5 days, +13 hours)
define('EXPIRATION_DATE', '+48 hours');

// Don't worry about this
define('CONTENT_TYPE','application/zip');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: ".date('U', strtotime(EXPIRATION_DATE)));

// The input string
$key = trim($_SERVER['QUERY_STRING']);

/*
 *	Retrive the keys
 */
$keys = file('keys/keys');
$match = false;

/*
 *	Loop through the keys to find a match
 *	When the match is found, remove it
 */
foreach($keys as &$one) {
    if(rtrim($one)==$key) {
        $match = true;
        $one = '';
    }
}

/*
 *	Puts the remaining keys back into the file
 */
file_put_contents('keys/keys',$keys);

/*
 * If we found a match
 */
if($match !== false) {

    /*
     *	Forces the browser to download a new file
     */
    $contenttype = CONTENT_TYPE;
    $filename = SUGGESTED_FILENAME;
    set_time_limit(0);
    header("Content-type: {$contenttype}");
    header("Content-Disposition: attachment; filename=\"{$filename}\"");
    readfile(PROTECTED_DOWNLOAD);

    // Exit
    exit;

} else {

    /*
     * 	We did NOT find a match
     *	OR the link expired
     *	OR the file has been downloaded already
     */
    echo "Download expired.";
} // end matching
?>